function delay(callback, time) {
	let lastCall = 0;

	return function (...args) {
		const now = new Date().getTime();

		if (now - lastCall < time) return;

		lastCall = now;

		return callback(...args);
	};
}

function debounce(callback, time) {
	let timeoutId;

	return function () {
		clearTimeout(timeoutId);

		timeoutId = setTimeout(() => {
			callback.apply(this, arguments);
		}, time);
	};
}

export { delay, debounce };
