import { debounce } from "./utils.js";

// Dom

const slider = document.querySelector('.slider');
const track = slider.querySelector('.slider__track');
const slides = track.querySelectorAll('.slider__slide');
const buttonNext = slider.querySelector('.slider__next');
const buttonPref = slider.querySelector('.slider__pref');
const dotBox = slider.querySelector('.slider__box-dots');
const dot = dotBox.querySelector('.slider__dot');

//Events

window.addEventListener('resize', init);
buttonNext.addEventListener('click', () => {
	nextSlide();
	handClick();
});

buttonPref.addEventListener('click', () => {
	prefSlide();
	handClick();
});

// Var

const conf = {
	automation: true,
	returnAutomation: true,

	time: {
		update: 100,
		switchingSlide: 7_000,
		returnAutoSwitchingSlides: 10_000,
	},
};

let index = 0;
const countSlides = slides.length;
let sliderWidth;
let timer;
let widthStatusDot = 0;
const factorStatusDot = 100 / (conf.time.switchingSlide / conf.time.update);

// Func

function init() {
	dotBox.innerHTML = '';
	sliderWidth = slider.offsetWidth;
	track.style.width = sliderWidth * countSlides + 'px';

	slides.forEach(handlerSlides);
	Array.from(dotBox.childNodes).forEach(handlerDots);

	clickNav();
	if (conf.automation) {
		clearTimer();
		startTimer();
	}
}

function handlerSlides(slide, index) {
	let cloneDot = dot.cloneNode(true);

	slide.style.width = sliderWidth + 'px';
	slide.dataset.id = index + 1;
	slide.addEventListener('click', clickSlideHandler);

	dotBox.appendChild(cloneDot);
}

function clickSlideHandler(event) {
	const id = this.dataset.id;
	alert(`Клик по слайду № ${id}`);
}

function handlerDots(dot, dotIndex) {
	dot.addEventListener('click', () => {
		index = dotIndex;
		drawSlide(index);
		drawActiveDot();
		handClick();
	});
}

function nextSlide() {
	index++;

	if (index >= slides.length) index = 0;
	clickNav();
}

function prefSlide() {
	index--;
	if (index < 0) index = slides.length - 1;

	clickNav();
}

function handClick() {
	if (conf.returnAutomation) checkTimer();
	clearTimer();
}

function clickNav() {
	widthStatusDot = 0;
	drawSlide(index);
	drawActiveDot();
}

function drawSlide(index) {
	track.style.transform = `translate(-${index * sliderWidth}px)`;
}

function drawActiveDot() {
	const dots = dotBox.querySelectorAll('.slider__dot');
	dots.forEach(dot => dot.classList.remove('slider__dot_active'));
	dots[index].classList.add('slider__dot_active');
}

function updateStatusSlide() {
	const dot = document.querySelector('.slider__dot_active .slider__dot-inner');

	widthStatusDot += factorStatusDot;

	if (widthStatusDot >= 100) {
		widthStatusDot = 0;
		drawStatusSlide(dot);
		nextSlide();
	}

	drawStatusSlide(dot);
}

function drawStatusSlide(dot) {
	dot.style.width = widthStatusDot + '%';
}

function startTimer() {
	timer = setInterval(update, conf.time.update);
}

function clearTimer() {
	widthStatusDot = 0;
	update();
	clearInterval(timer);
}

function update() {
	updateStatusSlide();
}

//

const checkTimer = debounce(startTimer, conf.time.returnAutoSwitchingSlides);

init();

// Test
